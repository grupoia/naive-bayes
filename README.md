# Implementação do algoritmo Naive Bayes em Java. #

## Para rodar o programa: ##

Esteja na pasta onde estão localizados:

*naivebayes\
*stopwords.txt

Feito isso, execute o comando

>java -Xmx2048m -Xms2048m naivebayes.SentimentIdentify [-c/-h/-s] <arquivo de dados com rotulo apenas>.csv <arquivo de saida>.txt

-c para Cross Validation
-h para Holdout
-s para remover Stop Words e Holdout

Licença GPL v3.0 - [Licença](http://www.gnu.org/copyleft/gpl.html)