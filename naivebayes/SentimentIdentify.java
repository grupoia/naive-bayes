package naivebayes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vtx-samsung-i5 on 11/21/14.
 */
public class SentimentIdentify {

    public static void main(String[] args) {


        if(args.length < 3){
          System.out.println("Como usar:\nDigite o tipo de programa, o nome de um arquivo .csv apropriado e um nome de arquivo para saida\n\njava SentimentIdentify [-n/-h/-s]<file>.csv <file>\n\nUse:\n\n -c para Cross Validation\n -s para remover Stopwords e usar Holdout\n -h para Holdout\n\nEntrada incorreta!");
        }else{

          if(args[0].equals("-h")){
              System.out.println("Criando arquivo '"+args[2]+"' e gerando saida para Holdout...");
              try{
                List<String> conjuntoDeDados = NaiveBayes.lerArquivo(args[1]);
                NaiveBayes.amostragemHoldout(conjuntoDeDados);
                NaiveBayes.gerarSaida(args[2]);

              }catch(IOException e){
                System.out.println("Arquivo nao encontrado!");
              }catch(Exception ex){
                System.out.println("Erro desconhecido!");
                ex.printStackTrace();
              }
              System.out.println("Concluido!");

          }else if(args[0].equals("-c")){
            System.out.println("Criando arquivo '"+args[2]+"' e gerando saida para Cross Validation...");
            try{
              List<String> conjuntoDeDados = NaiveBayes.lerArquivo(args[1]);
              NaiveBayes.executarCrossValidation(NaiveBayes.amostragemCrossValidation(conjuntoDeDados));
              NaiveBayes.gerarSaida(args[2]);

            }catch(IOException e){
              System.out.println("Arquivo nao encontrado!");
            }catch(Exception ex){
              ex.printStackTrace();
              System.out.println("Erro desconhecido!");
            }
            System.out.println("Concluido!");

          }else if(args[0].equals("-s")){
              System.out.println("Criando arquivo '"+args[2]+"', removendo StopWords e gerando saida para Holdout...");
              try{
                List<String> conjuntoDeDados = NaiveBayes.lerArquivo(args[1]);

                NaiveBayes.amostragemHoldoutSemStopWords(conjuntoDeDados);
                NaiveBayes.gerarSaida(args[2]);

              }catch(IOException e){
                System.out.println("Arquivo nao encontrado!");
              }catch(Exception ex){
                System.out.println("Erro desconhecido!");
              }
              System.out.println("Concluido!");
            }

          }

      }


    }
