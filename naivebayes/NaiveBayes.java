/**
 * ACH-2016 Inteligência Artificial - 2o Sem /2014
 * Profa. Patrícia R. Oliveira
 *
 * EP - Classificador Naive Bayes
 *
 * 8061855 - Tiago de Carvalho Miranda
 * ...
 *
 */
package naivebayes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NaiveBayes {
    //static double dGoodTweets, dBadTweets;
    static int goodTweets = 0, badTweets = 0;
    public static HashMap<String, Integer> vocabulario1 = new HashMap<String, Integer>();
    public static HashMap<String, Integer> vocabulario0 = new HashMap<String, Integer>();
    public static Set<String> vocabularioGeral = new HashSet<String>();
    public static HashMap<String, HashMap<Integer, Double>> prob = new HashMap<String, HashMap<Integer, Double>>();
    public static StringBuilder sb = new StringBuilder();

    static void countWords(List<String> treino, List<String> teste) {
        for(String tweet: teste) {
            tweet = tweet.substring(2);
            Scanner lerTweet = new Scanner(tweet);
            while (lerTweet.hasNext()) {
                vocabularioGeral.add(lerTweet.next());
            }
        }
        for (String tweet : treino) {
            int sentimento = Integer.parseInt(tweet.substring(0, 1));

            if (sentimento == 1) //Se for um tweet com bom sentimento, goodTweets++
                goodTweets++;
            else if (sentimento == 0)               //Outro caso basTweets++
                badTweets++;

            tweet = tweet.substring(2);

            Scanner lerTweet = new Scanner(tweet);

            while(lerTweet.hasNext()){

                String entrada = lerTweet.next();

                vocabularioGeral.add(entrada);

                if(sentimento == 1){
                    int contagem = 0;
                    if(vocabulario1.containsKey(entrada)){
                        contagem = vocabulario1.remove(entrada) + 1;
                        vocabulario1.put(entrada,contagem);
                    }else{
                        vocabulario1.put(entrada,1);
                    }
                }else if(sentimento == 0){
                    int contagem = 0;
                    if(vocabulario0.containsKey(entrada)){
                        contagem = vocabulario0.remove(entrada) + 1;
                        vocabulario0.put(entrada,contagem);
                    }else{
                        vocabulario0.put(entrada, 1);
                    }
                }

            }
        }
    }

    static void termosProbabilisticos(){
        /*Integer p = probabilities.get("love").get(0);
          Integer p = probabilities.get("love").get(1); */
        for(String t : vocabularioGeral){
            Integer n0 = 0, n1 = 0;
            if(vocabulario0.containsKey(t)){
                n0 = vocabulario0.get(t);
            }
            if(vocabulario1.containsKey(t)){
                n1 = vocabulario1.get(t);
            }
            HashMap<Integer, Double> p_per_class = new HashMap<Integer, Double>();
            p_per_class.put(0, (n0 + 1.0) / (badTweets + vocabularioGeral.size()));//1451219
            p_per_class.put(1, (n1 + 1.0) / (goodTweets + vocabularioGeral.size()));
            prob.put(t, p_per_class);
        }
    }

    public static Map<String, HashMap<Integer, Double>> calcularProbabilidades(List<String> textoRotulado){
        /*
            TODO
            Recebe uma lista com os documentos rotulados (tweets) e retorna um Map com as probabilidades a priori
        */
        double dGoodTweets = 0.0;
        double dBadTweets = 0.0;
        HashMap<String, Integer> vocab0 = new HashMap<String, Integer>();
        HashMap<String, Integer> vocab1 = new HashMap<String, Integer>();
        HashMap<String, Integer> vocabTodos = new HashMap<String, Integer>();

        Scanner scanTexto;

        Map<String, HashMap<Integer, Double>> probabilidadesPriori = new HashMap<String, HashMap<Integer, Double>>();

        for(String texto : textoRotulado){

            int sentimento = Integer.parseInt(texto.substring(0,1)); // Sentimento do texto ( 0 ou 1 )
            if (sentimento == 1) //Se for um tweet com bom sentimento, goodTweets++
                dGoodTweets++;
            else                //Outro caso basTweets++
                dBadTweets++;

            texto = texto.substring(2);

            scanTexto = new Scanner(texto);

            while(scanTexto.hasNext()){ // Iterando palavra a palavra o texto

                String entrada = scanTexto.next();
                int contagem = 0; // Contagem de palavras iguais

                if(vocabTodos.containsKey(entrada)){ //Adiciona palavra ao vocabulario geral
                    contagem = vocabTodos.remove(entrada) + 1;
                    vocabTodos.put(entrada,contagem);
                    contagem = 0;
                }else{
                    vocabTodos.put(entrada,1);
                }

                if(sentimento==1){//Adiciona ao vocabulario sentimento 1
                    if(vocab1.containsKey(entrada)){
                        contagem = vocab1.remove(entrada) + 1;
                        vocab1.put(entrada,contagem);
                        contagem = 0;
                    }else{
                        vocab1.put(entrada,1);
                    }
                }else{              //Adiciona ao vocabulario sentimento 0
                    if(vocab0.containsKey(entrada)){
                        contagem = vocab0.remove(entrada) + 1;
                        vocab0.put(entrada,contagem);
                        contagem = 0;
                    }else{
                        vocab0.put(entrada,1);
                    }
                }
            }
        }
        //Criados vocabularios total, sentimento 0 e sentimento 1

        for(String t : vocabTodos.keySet()){
            Integer n0 = 0, n1 = 0;
            if(vocab0.containsKey(t)){
                n0 = vocab0.get(t);
            }
            if(vocab1.containsKey(t)){
                n1 = vocab1.get(t);
            }
            HashMap<Integer, Double> p_per_class = new HashMap<Integer, Double>();
            p_per_class.put(0, (n0 + 1.0) / (badTweets + vocabTodos.size()));
            p_per_class.put(1, (n1 + 1.0) / (goodTweets + vocabTodos.size()));
            probabilidadesPriori.put(t, p_per_class);
        }

            HashMap<Integer, Double> p_per_class = new HashMap<Integer, Double>();
            p_per_class.put(0, dBadTweets  /vocabTodos.size());
            p_per_class.put(1, dGoodTweets /vocabTodos.size());
            probabilidadesPriori.put("PROGRAM_USAGE_PROB",p_per_class);//USO ESPECIAL DO PROGRAMA

        return probabilidadesPriori;
    }

    public static double testarClassificador(Map<String, HashMap<Integer, Double>> classificador, List<String> testeRotulado, List<String> testeNaoRotulado){
        /*
            Recebe o conjunto de teste rotulado (para confirmação) e não rotulado, além do classificador,
            realiza classificação e imprime o resultado (média acerto/erro)
        */

        int t0 = 0, t1 = 0, f0 = 0, f1 = 0;

        double acerto = 0.0;
        double erro = 0.0;

        int resposta = -1;

        double prob1 = 0.0;
        double prob0 = 0.0;

        Scanner scTweet;
        String palavra = "";

        for(int i = 0; i<testeNaoRotulado.size();i++){
            prob1 = 0.0;
            prob0 = 0.0;
            scTweet = new Scanner(testeNaoRotulado.get(i));

            while(scTweet.hasNext()){

                palavra = scTweet.next();
                //prob1 += Math.log((classificador.get("PROGRAM_USAGE_PROB")).get(1));
                //prob0 += Math.log((classificador.get("PROGRAM_USAGE_PROB")).get(0));
                if(classificador.containsKey(palavra)){

                    // Sentimento 1 e sentimento 0
                    prob1 += Math.log((classificador.get(palavra)).get(1));
                    prob0 += Math.log((classificador.get(palavra)).get(0));

                }

            }

            if(prob1 > prob0)
                resposta = 1;
            else
                resposta = 0;

            if(resposta == Integer.parseInt((testeRotulado.get(i)).substring(0,1))){
                if(resposta == 1)
                    t1++;
                else
                    t0++;
                acerto++;
            }
            else{
                if(resposta == 1)
                    f1++;
                else
                    f0++;
                erro++;
            }


/*
            if(resposta == Integer.parseInt((testeRotulado.get(i)).substring(0,1)))
                acerto++;
            else
                erro++;
/*
            System.out.println(resposta+","+testeRotulado.get(i).substring(0,1));
            System.out.println(prob0 + " - " + prob1);
*/

        }
        sb.append("\nResultados\nAcertos:"+acerto+"\nErros:"+erro);//---------
        sb.append("\n\nAcuracia: "+(1-(erro/(acerto+erro)))+"\n");//---------------

        String matriz = "";
        matriz += "********Matriz de confusao********";
        matriz += "\nClasse                 Verdadeira 0        |  Verdadeira 1";
        matriz += "\nClasse prevista 0      " + t0 + "("+ t0 * 1.0 / (erro + acerto) * 100 + "%)" + "   |  " + "     " + f0  + "("+ f0 * 1.0 / (erro + acerto) * 100 + "%)" ;
        matriz += "\nClasse prevista 1      " + f1  + "("+ f1 * 1.0 / (erro + acerto) * 100 + "%)" + "   |  " + "     " + t1  + "("+ t1 * 1.0 / (erro + acerto) * 100 + "%)" ;        sb.append(matriz);
        return erro/(acerto+erro);
    }

    public static Map<String, HashMap<Integer, Double>> removeStopWords(Map<String, HashMap<Integer, Double>> classificador){

      List<String> stopWords = new ArrayList<String>();

    //  System.out.println(classificador.size());

      try{
          FileReader fis = new FileReader(/*"TestBase.csv"*/"stopwords.txt");
          BufferedReader buff = new BufferedReader(fis);
          Scanner sc = new Scanner(buff);
          while(sc.hasNextLine()) {
              stopWords.add(sc.nextLine());
          }
      }catch(Exception e){
        e.printStackTrace();
          System.out.println("Arquivo de stopwords não encontrado!\n\nColocar aquivo 'stopwords.txt' na mesma pasta que o programa!");
      }
        Map<String, HashMap<Integer, Double>> classificadorNovo = classificador;
        for(String s : stopWords){
          classificadorNovo.remove(s);
        }

      //  System.out.println(classificadorNovo.size());

        return classificadorNovo;
  }

    public static List<List> amostragemCrossValidation(List<String> testeRotulado){

      int totalNumber = testeRotulado.size();
      Random r = new Random();
      int inicial = r.nextInt(totalNumber-1);
      int foldSize = totalNumber/10;
      int totalIndex = 0;
      List<List> dezFoldsComRotulo = new ArrayList<List>();

      sb.append("\n                                   _ _     _       _   _             \n");
      sb.append("  ___ _ __ ___  ___ ___  __   ____ _| (_) __| | __ _| |_(_) ___  _ __  \n");
      sb.append(" / __| '__/ _ \\/ __/ __| \\ \\ / / _` | | |/ _` |/ _` | __| |/ _ \\| '_ \\ \n");
      sb.append("| (__| | | (_) \\__ \\__ \\  \\ V / (_| | | | (_| | (_| | |_| | (_) | | | |\n");
      sb.append(" \\___|_|  \\___/|___/___/   \\_/ \\__,_|_|_|\\__,_|\\__,_|\\__|_|\\___/|_| |_|\n");
      sb.append("\n\n");
      sb.append("\nTotal de registros: "+totalNumber);
      sb.append("\nTamanho do fold: "+foldSize+"\n\n");

      int randPosition = 0;

      for(int fold = 0;fold<10;fold++){
        List<String> umFoldComRotulo = new ArrayList<String>();

        for(int tam = 0; tam<foldSize;tam++){
          randPosition = r.nextInt(testeRotulado.size()-1);
          umFoldComRotulo.add(testeRotulado.remove(randPosition));
        }
          dezFoldsComRotulo.add(fold,umFoldComRotulo);

      }

      //Caso o numero de itens nos folds não tenha sido exato
      while(testeRotulado.size()>0){
        (dezFoldsComRotulo.get(9)).add(testeRotulado.remove(0));
      }

      return dezFoldsComRotulo;
    }

    public static double executarCrossValidation(List<List> dezFoldsComRotulo){

      double totalNumber = 0.0;
      for(List l : dezFoldsComRotulo){
        totalNumber += l.size();
      }

      List<String> listaTesteComRotulo = new ArrayList<String>();
      List<String> listaTesteSemRotulo = new ArrayList<String>();
      List<String> listaTreino = new ArrayList<String>();
      int train = 0;
      double mediaErro = 0.0;
      for(int fold = 0; fold<10;fold++){//Treinando o classificador e testando
        sb.append("\nFOLD NÚMERO -> "+fold);//---------
        train = fold;
        while(true){
          train = (train+1)%10;
          if(train==fold)
            break;
          listaTreino.addAll(dezFoldsComRotulo.get(train));
        }
          listaTesteComRotulo.addAll(dezFoldsComRotulo.get(fold));

          for(String s : (List<String>)dezFoldsComRotulo.get(fold)){
            listaTesteSemRotulo.add(s.substring(2));
          }

          mediaErro +=testarClassificador(calcularProbabilidades(listaTreino),listaTesteComRotulo,listaTesteSemRotulo);
          listaTesteComRotulo.clear();
          listaTesteSemRotulo.clear();
          listaTreino.clear();
        }

      mediaErro = mediaErro/10;
      sb.append("\n_______________________________________________________________");
      sb.append("\n\nMedia do Erro\n\n"+mediaErro);//--------------
      sb.append("\n\nAcuracia\n\n"+(1-mediaErro));//--------------------
      double erroPadrao = Math.sqrt((mediaErro*(1-mediaErro))/totalNumber);
      sb.append("\n\nErro Padrao\n\n"+erroPadrao);//------------------

      return mediaErro;
    }

    public static double amostragemHoldoutSemStopWords(List<String> textoRotulado){
        Random rObject = new Random();

        List<String> tweets = textoRotulado;
        List<String> teste = new ArrayList<String>();
        List<String> treino = new ArrayList<String>();
        List<String> testeSemRotulo = new ArrayList<String>();

        int nTest = textoRotulado.size()/3;// nTest > Tamanho do conjunto de teste
        int n = textoRotulado.size(); // n > Tamanho do conjunto de dados passado
        int r = rObject.nextInt(n-1);

        sb.append("\n\n _                                   _     \n");
        sb.append(" ___| |_ ___  _ ____      _____  _ __ __| |___  \n");
        sb.append("/ __| __/ _ \\| '_ \\ \\ /\\ / / _ \\| '__/ _` / __|\n");
        sb.append("\\__ \\ || (_) | |_) \\ V  V / (_) | | | (_| \\__ \\\n");
        sb.append("|___/\\__\\___/| .__/ \\_/\\_/ \\___/|_|  \\__,_|___/\n");
        sb.append("             |_|                               \n\n\n");


        //Gerando o teste e o treino
        int rNumber = 0;
        for(int test = 0; test<nTest;test++){
          //gerando teste
          rNumber = rObject.nextInt(textoRotulado.size());
          String s = textoRotulado.remove(rNumber);
          
          teste.add(s);
          testeSemRotulo.add(s.substring(2));
        }
        while(textoRotulado.size()>0){
            //gerando treino
            treino.add(textoRotulado.remove(0));
        }

        double erro = testarClassificador(removeStopWords(calcularProbabilidades(treino)),teste,testeSemRotulo);
        sb.append("\nErro:"+erro+"\n");
      return erro;
    }

    public static double amostragemHoldout(List<String> textoRotulado) {
        Random rObject = new Random();

        List<String> tweets = textoRotulado;
        List<String> teste = new ArrayList<String>();
        List<String> treino = new ArrayList<String>();
        List<String> testeSemRotulo = new ArrayList<String>();

        int nTest = textoRotulado.size()/3;// nTest > Tamanho do conjunto de teste
        int n = textoRotulado.size(); // n > Tamanho do conjunto de dados passado
        int r = rObject.nextInt(n-1);

        sb.append(" _           _     _             _   \n");
        sb.append("| |__   ___ | | __| | ___  _   _| |_ \n");
        sb.append("| '_ \\ / _ \\| |/ _` |/ _ \\| | | | __|\n");
        sb.append("| | | | (_) | | (_| | (_) | |_| | |_ \n");
        sb.append("|_| |_|\\___/|_|\\__,_|\\___/ \\__,_|\\__|\n");

        //Gerando o teste e o treino
        int rNumber = 0;
        for(int test = 0; test<nTest;test++){
          //gerando teste
          rNumber = rObject.nextInt(textoRotulado.size());
          String s = textoRotulado.remove(rNumber);
          
          teste.add(s);
          testeSemRotulo.add(s.substring(2));
        }
        while(textoRotulado.size()>0){
            //gerando treino
            treino.add(textoRotulado.remove(0));
        }

        double erro = testarClassificador(calcularProbabilidades(treino),teste,testeSemRotulo);
        sb.append("\nErro:"+erro+"\n");
      return erro;
    }


public static ArrayList<String> lerArquivo (String arquivo) throws IOException{

    	ArrayList<String> listaTweets = new ArrayList<String>();

    	try{
    		BufferedReader br = new BufferedReader(
					new FileReader(arquivo));
    		while(br.ready()){
    			String tweet = br.readLine();
    			listaTweets.add(tweet);
    		}

    	}
    	catch (FileNotFoundException ex) {
			Logger.getLogger(NaiveBayes.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(NaiveBayes.class.getName()).log(Level.SEVERE, null, ex);
		}

    	return listaTweets;
    }

    public static void gerarSaida(String arquivo){
    	try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(
					arquivo, true));
			bw.write(sb.toString());
			bw.flush();
			bw.close();
		} catch (IOException ex) {
			Logger.getLogger(NaiveBayes.class.getName()).log(Level.SEVERE, null, ex);
		}
    }

}
